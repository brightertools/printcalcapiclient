﻿namespace PrintCalcApiClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.UsernameTextBox = new System.Windows.Forms.TextBox();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.SessionKeyTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ApiRootUrlTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.UploadFileButton = new System.Windows.Forms.Button();
            this.FilePathTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SelectFileButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label7 = new System.Windows.Forms.Label();
            this.FileIdTextBox = new System.Windows.Forms.TextBox();
            this.GetResultsButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.GetSessionKeyButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ApiKeyTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.PreProcessingSelectList = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password:";
            // 
            // UsernameTextBox
            // 
            this.UsernameTextBox.Location = new System.Drawing.Point(59, 142);
            this.UsernameTextBox.Name = "UsernameTextBox";
            this.UsernameTextBox.Size = new System.Drawing.Size(497, 20);
            this.UsernameTextBox.TabIndex = 3;
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Location = new System.Drawing.Point(59, 181);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.Size = new System.Drawing.Size(497, 20);
            this.PasswordTextBox.TabIndex = 4;
            // 
            // SessionKeyTextBox
            // 
            this.SessionKeyTextBox.Location = new System.Drawing.Point(59, 257);
            this.SessionKeyTextBox.Name = "SessionKeyTextBox";
            this.SessionKeyTextBox.Size = new System.Drawing.Size(497, 20);
            this.SessionKeyTextBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Session Key:";
            // 
            // ApiRootUrlTextBox
            // 
            this.ApiRootUrlTextBox.Location = new System.Drawing.Point(59, 63);
            this.ApiRootUrlTextBox.Name = "ApiRootUrlTextBox";
            this.ApiRootUrlTextBox.Size = new System.Drawing.Size(497, 20);
            this.ApiRootUrlTextBox.TabIndex = 8;
            this.ApiRootUrlTextBox.Text = "http://www.printcalc.com/api/v1/";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Api Root Url:";
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Location = new System.Drawing.Point(59, 346);
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(150, 20);
            this.DescriptionTextBox.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 330);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "File Description:";
            // 
            // UploadFileButton
            // 
            this.UploadFileButton.Location = new System.Drawing.Point(59, 410);
            this.UploadFileButton.Name = "UploadFileButton";
            this.UploadFileButton.Size = new System.Drawing.Size(150, 23);
            this.UploadFileButton.TabIndex = 13;
            this.UploadFileButton.Text = "Upload File";
            this.UploadFileButton.UseVisualStyleBackColor = true;
            this.UploadFileButton.Click += new System.EventHandler(this.UploadFileButton_Click);
            // 
            // FilePathTextBox
            // 
            this.FilePathTextBox.Location = new System.Drawing.Point(215, 346);
            this.FilePathTextBox.Name = "FilePathTextBox";
            this.FilePathTextBox.Size = new System.Drawing.Size(307, 20);
            this.FilePathTextBox.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(212, 330);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "File Path:";
            // 
            // SelectFileButton
            // 
            this.SelectFileButton.Location = new System.Drawing.Point(528, 346);
            this.SelectFileButton.Name = "SelectFileButton";
            this.SelectFileButton.Size = new System.Drawing.Size(28, 23);
            this.SelectFileButton.TabIndex = 16;
            this.SelectFileButton.Text = "...";
            this.SelectFileButton.UseVisualStyleBackColor = true;
            this.SelectFileButton.Click += new System.EventHandler(this.SelectFileButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(55, 478);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(241, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "File Id (auto filled by step 2 on successful upload):";
            // 
            // FileIdTextBox
            // 
            this.FileIdTextBox.Location = new System.Drawing.Point(58, 494);
            this.FileIdTextBox.Name = "FileIdTextBox";
            this.FileIdTextBox.Size = new System.Drawing.Size(238, 20);
            this.FileIdTextBox.TabIndex = 17;
            // 
            // GetResultsButton
            // 
            this.GetResultsButton.Location = new System.Drawing.Point(60, 520);
            this.GetResultsButton.Name = "GetResultsButton";
            this.GetResultsButton.Size = new System.Drawing.Size(149, 23);
            this.GetResultsButton.TabIndex = 19;
            this.GetResultsButton.Text = "Get File Results";
            this.GetResultsButton.UseVisualStyleBackColor = true;
            this.GetResultsButton.Click += new System.EventHandler(this.GetResultsButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(496, 520);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(60, 23);
            this.CloseButton.TabIndex = 20;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // GetSessionKeyButton
            // 
            this.GetSessionKeyButton.Location = new System.Drawing.Point(59, 207);
            this.GetSessionKeyButton.Name = "GetSessionKeyButton";
            this.GetSessionKeyButton.Size = new System.Drawing.Size(150, 23);
            this.GetSessionKeyButton.TabIndex = 21;
            this.GetSessionKeyButton.Text = "Get Session Key";
            this.GetSessionKeyButton.UseVisualStyleBackColor = true;
            this.GetSessionKeyButton.Click += new System.EventHandler(this.GetSessionKeyButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 31);
            this.label8.TabIndex = 22;
            this.label8.Text = "1)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 289);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 31);
            this.label9.TabIndex = 23;
            this.label9.Text = "2)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 441);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 31);
            this.label10.TabIndex = 24;
            this.label10.Text = "3)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(56, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "API Key:";
            // 
            // ApiKeyTextBox
            // 
            this.ApiKeyTextBox.Location = new System.Drawing.Point(59, 102);
            this.ApiKeyTextBox.Name = "ApiKeyTextBox";
            this.ApiKeyTextBox.Size = new System.Drawing.Size(497, 20);
            this.ApiKeyTextBox.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(53, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(503, 22);
            this.label12.TabIndex = 27;
            this.label12.Text = "Enter API Key, Username, Password to Generate SessionKey";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(53, 296);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(336, 22);
            this.label13.TabIndex = 28;
            this.label13.Text = "Enter Description && Select File to Upload";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(55, 450);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(210, 22);
            this.label14.TabIndex = 29;
            this.label14.Text = "Enter File Id, Get Results";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(58, 369);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Pre-processing:";
            // 
            // PreProcessingSelectList
            // 
            this.PreProcessingSelectList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PreProcessingSelectList.FormattingEnabled = true;
            this.PreProcessingSelectList.Items.AddRange(new object[] {
            "(none)",
            "Convert to CMYK",
            "Convert to Grayscale"});
            this.PreProcessingSelectList.Location = new System.Drawing.Point(60, 383);
            this.PreProcessingSelectList.Name = "PreProcessingSelectList";
            this.PreProcessingSelectList.Size = new System.Drawing.Size(148, 21);
            this.PreProcessingSelectList.TabIndex = 31;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 562);
            this.Controls.Add(this.PreProcessingSelectList);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.ApiKeyTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.GetSessionKeyButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.GetResultsButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.FileIdTextBox);
            this.Controls.Add(this.SelectFileButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.FilePathTextBox);
            this.Controls.Add(this.UploadFileButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ApiRootUrlTextBox);
            this.Controls.Add(this.SessionKeyTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PasswordTextBox);
            this.Controls.Add(this.UsernameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintCalc Api Test Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox UsernameTextBox;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.TextBox SessionKeyTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ApiRootUrlTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button UploadFileButton;
        private System.Windows.Forms.TextBox FilePathTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button SelectFileButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox FileIdTextBox;
        private System.Windows.Forms.Button GetResultsButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button GetSessionKeyButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox ApiKeyTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox PreProcessingSelectList;
    }
}

