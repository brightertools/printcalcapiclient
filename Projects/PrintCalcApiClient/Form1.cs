﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;

namespace PrintCalcApiClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SelectFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                FilePathTextBox.Text = openFileDialog1.FileName;
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UploadFileButton_Click(object sender, EventArgs e)
        {
            using (var client = new HttpClient())
            {
                // Headers for authorization
                client.DefaultRequestHeaders.Add("Username", UsernameTextBox.Text);
                client.DefaultRequestHeaders.Add("SessionKey", SessionKeyTextBox.Text);

                using (var formData = new MultipartFormDataContent())
                {
                    var stringContent = new StringContent(DescriptionTextBox.Text);
                    stringContent.Headers.Add("Content-Disposition", "form-data; name=\"Description\"");
                    formData.Add(stringContent, "Description");

                    var stringContent2 = new StringContent(PreProcessingSelectList.SelectedIndex.ToString(CultureInfo.InvariantCulture));
                    stringContent2.Headers.Add("Content-Disposition", "form-data; name=\"PreProcessingOption\"");
                    formData.Add(stringContent2, "PreProcessingOption");

                    var fileContent = new ByteArrayContent(File.ReadAllBytes(FilePathTextBox.Text));
                    fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = Path.GetFileName(FilePathTextBox.Text)
                    };
                    formData.Add(fileContent);

                    var response = client.PostAsync(ApiRootUrlTextBox.Text + "UploadFile", formData).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        FileIdTextBox.Text = response.Content.ReadAsStringAsync().Result;
                        MessageBox.Show("File Uploaded");
                        return;
                    }
                    MessageBox.Show("Error: " + response.ReasonPhrase);
                }
            }
        }

        private void GetResultsButton_Click(object sender, EventArgs e)
        {
            using (var client = new HttpClient())
            {
                // Headers for authorization
                client.DefaultRequestHeaders.Add("Username", UsernameTextBox.Text);
                client.DefaultRequestHeaders.Add("SessionKey", SessionKeyTextBox.Text);

                var response = client.PostAsync(ApiRootUrlTextBox.Text + "GetFileResults?Id=" + FileIdTextBox.Text, null).Result;

                if (response.IsSuccessStatusCode)
                {
                    var results = response.Content.ReadAsStringAsync().Result;
                    MessageBox.Show(results);
                    return;
                }
                MessageBox.Show("Error: " + response.ReasonPhrase);
            }
        }

        private void GetSessionKeyButton_Click(object sender, EventArgs e)
        {
            using (var client = new HttpClient())
            {
                // Headers for authorization
                client.DefaultRequestHeaders.Add("Username", UsernameTextBox.Text);
                client.DefaultRequestHeaders.Add("SessionKey", SessionKeyTextBox.Text);

                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("ApiKey", ApiKeyTextBox.Text),
                    new KeyValuePair<string, string>("Username", UsernameTextBox.Text),
                    new KeyValuePair<string, string>("Password", PasswordTextBox.Text)
                });


                var response = client.PostAsync(ApiRootUrlTextBox.Text + "GetSessionKey", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    SessionKeyTextBox.Text = response.Content.ReadAsStringAsync().Result;
                    return;
                }
                MessageBox.Show("Error: " + response.ReasonPhrase);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PreProcessingSelectList.SelectedIndex = 0;
        }
    }
}